classdef exampleSimulation < monty
    properties
        %inputs and default values
        ebnodb     = 1;   %BIAWGN Channel SNR using Eb/N0, in dB
        n          = 7;   %code block length
        k          = 4;   %code dimension
        
        %outputs
        varChan;          %computed from ebnodb
        nBitError  = 0;
        nWordError = 0;
        nWord      = 0;
        BER        = Inf; %bit error rate. Set Inf helps with plotting
        WER        = Inf; %word error rate.
    end
    
    properties (Hidden)
        G;                   %code generator matrix
        codebook;            %codebook for ML decoder
        plotvar_x = 'ebnodb' %fields for plotting
        plotvar_y = 'BER'
    end
    
    methods
        function curve = exampleSimulation(varargin)
            %EXAMPLESIMULATION   monty example simulation
            %
            %  EXAMPLESIMULATION is an example of a class for use
            %  with monty. Use this example as a starting point for
            %  writing you own simulation.
            %
            %  The simulation uses a maximum-likelihood decoder to
            %  decode Hamming (7,4) codewords transmitted over a
            %  BIAWGN channel. Example:
            %
            %  monty.generate('dataFile',exampleSimulation,...
            %    'ebnodb',{1 2 3})
            %  monty.run('exampleData')
            %
            
            %continue condition: if string evaluates true, then
            %simulation will continue.  Otherwise, simulation stops
            curve.montyContinue = 'curve.nWordError < 10';
            curve.montyAddField = {'nBitError','nWordError','nWord'};
            curve.montyConcatField = { };
            
            %process the input arguments. For example,
            %exampleSimulation('k',3) overrides default 'k' value
            curve = curve.inputArguments(varargin{:});
            
            %Hamming (7,4) generator matrix and codebook generation
            curve.G = [...
                1 0 0 0 1 1 1; ...
                0 1 0 0 1 1 0; ...
                0 0 1 0 1 0 1; ...
                0 0 0 1 0 1 1]';
            for ii = 1:2^curve.k
                u = de2bi(ii-1,curve.k)';
                curve.codebook(:,ii) = curve.G * u;
            end
        end
        
        function curve = simulate(curve)
            %SIMULATION CORE
            R             = curve.k / curve.n;
            curve.varChan = (0.5/(2*R)) * 10^(- curve.ebnodb / 10);
            
            while (curve.montyErrorLimit)
                u = randi(2,curve.k,1) - 1;
                x = curve.G * u;
                z = randn(curve.n,1) * sqrt(curve.varChan);
                y = x + z;
                
                %maximum-likelihood decoder
                bestDist = Inf;
                for ii = 1:size(curve.codebook,2)
                    dist = norm(y - curve.codebook(:,ii));
                    if dist < bestDist
                        bestDist = dist;
                        xhat     = curve.codebook(:,ii);
                    end
                end
                
                ne   = length(find(xhat ~= x));
                curve.nBitError    = curve.nBitError + ne;
                curve.nWordError   = curve.nWordError + (ne > 0);
                curve.nWord        = curve.nWord + 1;
            end
        end
        
        %Monty will calculate error rates after completion
        function curve = calculate(curve)
            curve.BER = curve.nBitError / (curve.nWord * curve.n);
            curve.WER = curve.nWordError / curve.nWord ;
        end
        
    end
end
